import heapq

class Graph:
    def __init__(self, directed=True):
        self.vertices = []
        self.nodes = []
        self.degrees = []
        self.size = 0
        self.directed = directed

    def __repr__(self):
        return str({self.vertices[k]: [(self.vertices[x], y) for x, y in v] \
            for k, v in enumerate(self.nodes)})

    def add_vertex(self, *keys):
        for key in keys:
            if key not in self.vertices:
                self.vertices.append(key)
                self.nodes.append([])
                self.degrees.append(0)
                self.size += 1

    def add_edge(self, a, b, weight=0):
        if a not in self.nodes:
            self.add_vertex(a)
        if b not in self.nodes:
            self.add_vertex(b)

        _a, _b = self.vertices.index(a), self.vertices.index(b)
        self.nodes[_a].append((_b, weight))

        self.degrees[_b] += 1

        if not self.directed:
            self.nodes[_b].append((_a, weight))
            self.degrees[_a] += 1

    def dijkstra(self, a, b=None):
        _a = self.vertices.index(a)
        _b = self.vertices.index(b) if b is not None else None

        d = [float('inf') for _ in xrange(self.size)]
        d[_a] = 0.

        q = [(d[_a], _a)]
        while q:
            dist, u = heapq.heappop(q)

            if u == _b:
                return dist

            for v, weight in self.nodes[u]:
                tmp = d[u] + weight
                if d[v] > tmp:
                    d[v] = tmp
                    heapq.heappush(q, (tmp, v))

        if _b is None: # List all distances from a
            return d
        else:
            return d[_b]

    def prim(self, a):
        G = Graph(directed=False)

        _a = self.vertices.index(a)

        d = [float('inf') for _ in xrange(self.size)]
        d[_a] = 0
        parent = [i for i in xrange(self.size)]

        q = [(0, _a)] # Distance 0 for node index 0
        while q:
            dist, u = heapq.heappop(q)
            for v, weight in self.nodes[u]:
                if d[v] > weight:
                    d[v] = weight
                    parent[v] = u
                    heapq.heappush(q, (weight, v))

        for i in xrange(self.size):
            G.add_vertex(self.vertices[i])
            if i != 0:
                G.add_edge(self.vertices[i], self.vertices[parent[i]], d[i])

        return G

    def connected_components(self):
        """Computes the list of connected components of the graph."""
        components = []
        to_visit = set([i for i in range(self.size)])

        while to_visit:
            # We choose a random node
            i = to_visit.pop()
            # We create an empty connected component
            cc = set([])
            # We'll further use a stack for the search
            st = []
            
            # A classic DFS
            st.append(i)
            while st:
                curr = st.pop()
                to_visit.discard(curr)
                cc.add(curr)
                for n, _ in self.nodes[curr]:
                    if n in to_visit:
                        st.append(n)

            components.append(cc)

        return components # TODO: return a list of graph instead of simple sets.
    
    def dot_export(self, filepath='./export.dot', name='MyAwesomeGraph', keys=True, values=True):
        """Exports the graph in a Dot file."""
        
        graph_type, link = ('digraph', '->') if self.directed else ('graph', '--')
        
        with open(filepath, 'w') as h:
            
            
            h.write('{} {} '.format(graph_type, name) + '{\n')
            
            if keys: #TODO: lambda.
                for n, succs in enumerate(self.nodes):
                    for succ, w in succs: 
                        h.write('\t{} {} {} {};\n'.format(self.vertices[n], link, self.vertices[succ], \
                                                              '[label={}]'.format(w) if values else ''))
                        
            else:
                for n, succs in enumerate(self.nodes):
                    for succ, w in succs: 
                       h.write('\t{} {} {} {};\n'.format(n, link, succ, \
                                                             '[label={}]'.format(w) if values else '')) 
                        
            h.write('}')

    def 
             
             

if __name__ == '__main__':
    G = Graph(directed=False)
    G.add_vertex('A', 'B', 'C', 'D')
    G.add_edge('A', 'B', 2.5)
    G.add_edge('A', 'C', 4.)
    G.add_edge('A', 'D', 1.)
    G.add_edge('B', 'D', 1.)
    G.add_edge('B', 'C', 1.)
    G.add_edge('X', 'Y', 42.)

    print G

    print G.degrees

    print G.dijkstra('C')

    print G.prim('A')

    print G.connected_components()

    G.prim('A').dot_export()

