import Graph

class DiGraph(Graph.Graph):
    '''Directed graph class representation & algorithms.'''

    def __init__(self):
        super(DiGraph, self).__init__()
        self.adjacency_lists = []

    def __repr__(self):
        return str(self.as_dict())

    def as_dict(self):
        return {self.indexes_to_keys[i]: neighbors \
                    for i, neighbors in enumerate(self.adjacency_lists)}

    def add_vertex(self, *keys):
        super(DiGraph, self).add_vertex(*keys)
        for _ in keys:
            self.adjacency_lists.append([])

    def add_edge(self, a, b, w=0):
        if a not in self.keys_to_indexes:
            self.add_vertex(a)

        if b not in self.keys_to_indexes:
            self.add_vertex(b)

        _a, _b = self.keys_to_indexes[a], self.keys_to_indexes[b]
        self.adjacency_lists[_a].append((_b, w))
        self.degrees[_a] += 1
    
    def neighbors(self, vertex, keys=True):
        '''Returns the neighbors (vertex, weight) of a
        vertex as a generator.'''

        if keys:
            _vertex = self.keys_to_indexes[vertex]
        else:
            _vertex = vertex

        return (neighbor for neighbor in self.adjacency_lists[_vertex])

if __name__ == '__main__':
    G = DiGraph()

    G.add_vertex('A', 'B', 'C', 'D')

    G.add_edge('A', 'B', 2.5)
    G.add_edge('A', 'C', 1.)
    G.add_edge('B', 'D', 4.2)
    G.add_edge('C', 'E', 1.3)

    G.add_edge('X', 'Y', 42.)

    print G
    print G.degrees

    print list(G.weight('A', 'B', keys=True))

    print list(G.bfs('A', keys=True))
    print list(G.dfs('A', keys=True))

    print G.dijkstra(0, keys=False) # Dijkstra starting from 'A' to all vertices
    print G.dijkstra('A', 'D')
