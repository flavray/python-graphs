import Graph

class UnDiGraph(Graph.Graph):
    '''Undirected graph class representation and algorithms.
    
    TODO: stop using self.size to generate indexes and so, this is
    ad-hoc and introduces bugs. Believe me.
    '''

    def __init__(self):
        '''In order to avoid excessive resizing of the adjacency
        matrix, we would like to roughly know by advance the size of
        the graph. N is by default set to 0. TODO (self.size used in
        self.degrees modification to fix).

        Note that self.adjacency_matrix is not really a matrix. Since
        the AM of an undirected graph is symmetric, we only store the
        bottom half.'''
        
        super(UnDiGraph, self).__init__()
        #self.adjacency_matrix = [[[] for _ in xrange(i)] for i in xrange(N)]
        #self.size = N
        self.adjacency_matrix = []
        
    def __repr__(self):
        #return str(self.as_dict())
        return str(self.adjacency_matrix)

    def as_dict(self):
        return {self.indexes_to_keys[i]: \
                    filter(lambda l: not (l == []), self.adjacency_matrix[i]) \
                    for i in xrange(self.size)}

    def add_vertex(self, *keys):
        super(UnDiGraph, self).add_vertex(*keys)
        for i, _ in enumerate(keys):
            self.adjacency_matrix.append( \
                [[] for _ in xrange(self.size - len(keys) + i + 1)]) # DIRTY AS FUCK.
        
    def add_edge(self, a, b, w=0):
        '''Invariant : we always lookup and modify the matrix in a, b;
        given that a > b.'''

        if a not in self.keys_to_indexes:
            self.add_vertex(a)

        if b not in self.keys_to_indexes:
            self.add_vertex(b)

        _a, _b = self.keys_to_indexes[a], self.keys_to_indexes[b]

        # We retablish here the invariant.
        _a, _b = max(_a, _b), min(_a, _b)

        self.adjacency_matrix[_a][_b].append(w)

        self.degrees[_a] += 1
        self.degrees[_b] += 1

    def neighbors(self, vertex, keys=True):
        '''Returns the neighbors of a vertex as a generator.'''
        
        if keys:
            _vertex = self.keys_to_indexes[vertex]
        else:
            _vertex = vertex

        for i, l in enumerate(self.adjacency_matrix[_vertex]):
            for w in l:
                yield (i, w)


if __name__ == '__main__':
    G = UnDiGraph()
    G.add_vertex('A', 'B', 'C', 'D')

    G.add_edge('A', 'B', 2.5)
    G.add_edge('A', 'C', 1.)
    G.add_edge('B', 'D', 4.2)
    G.add_edge('C', 'E', 1.3)

    G.add_edge('X', 'Y', 42.)

    print G
    print G.degrees

    print list(G.weight('A', 'B', keys=True)) # Fails, to be fixed.

    print list(G.bfs('A', keys=True))
    print list(G.dfs('A', keys=True))

    print G.dijkstra(0, keys=False) # Dijkstra starting from 'A' to all vertices
    print G.dijkstra('A', 'D') # Fails as well.
   

    
