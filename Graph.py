import heapq

from collections import deque

class Graph(object):
    '''Main class for a general graph. It shall not be instanciated.'''

    def __init__(self):
        self.size = 0
        self.degrees = []
        self.indexes_to_keys = []
        self.keys_to_indexes = {}

    def add_vertex(self, *args):
        '''Adds a vertex to the graph.'''

        for key in args:
            if key not in self.keys_to_indexes:
                self.degrees.append(0)
                self.indexes_to_keys.append(key)
                self.keys_to_indexes[key] = self.size
                self.size += 1

    def weight(self, a, b, keys=True):
        '''Given two vertices a and b, returns the weights of the
        existing edges (a, b).'''

        if keys:
            _a = self.keys_to_indexes[a]
            _b = self.keys_to_indexes[b]
        else:
            _a, _b = a, b

        for neighbor, w in self.neighbors(_a, keys=False):
            if neighbor == _b:
                yield w

    def bfs(self, root, keys=True):
        '''Simple breadth-first-search starting from root.'''

        if keys:
            _root = self.keys_to_indexes[root]
        else:
            _root = root

        visited = [False] * self.size
        visited[_root] = True

        queue = deque([_root])
        while queue:
            v = queue.popleft()
            yield v if not keys else self.indexes_to_keys[v]

            for neighbor, _ in self.neighbors(v, keys=False):
                if not visited[neighbor]:
                    visited[neighbor] = True
                    queue.append(neighbor)

    def dfs(self, root, keys=True):
        '''Simple depth-first-search starting from root.'''

        if keys:
            _root = self.keys_to_indexes[root]
        else:
            _root = root

        visited = [False] * self.size
        visited[_root] = True

        stack = [_root]
        while stack:
            v = stack.pop()
            yield v if not keys else self.indexes_to_keys[v]

            for neighbor, _ in self.neighbors(v, keys=False):
                if not visited[neighbor]:
                    visited[neighbor] = True
                    stack.append(neighbor)

    def dijkstra(self, start, finish=None, keys=True):
        '''Dijkstra shortest path algorithm.'''

        if keys:
            _start = self.keys_to_indexes[start]
            if finish is not None:
                _finish = self.keys_to_indexes[finish]
            else:
                _finish = None
        else:
            _start, _finish = start, finish

        d = [float('inf') for _ in xrange(self.size)]
        d[_start] = 0.

        heap = [(d[_start], _start)]
        while heap:
            dist, u = heapq.heappop(heap)

            if u == finish:
                return dist

            for v, w in self.neighbors(u, keys=False):
                tmp = d[u] + w
                if d[v] > tmp:
                    d[v] = tmp
                    heapq.heappush(heap, (tmp, v))

        if _finish is not None:
            return d[_finish]
        else:
            if keys:
                return [(k, d[i]) for i, k in enumerate(self.indexes_to_keys)]
            else:
                return d

